variable "ci_user" {
    type = string
}
variable "ci_pass" {
    type = string
}
variable "image" {
    type = string
}
job "user-service" {
    datacenters = ["dc1"]
    type = "service"
    group "user-service" {
        network {
            port "app" {}
        }
        service {
            name = "user-service"
            port = "app"
        }
        task "user-service" {
			vault {
				policies = ["user-service"]
			}
            template {
                data = file("nomad/config.env.tpl")
                destination = "secrets/config.env"
                env = true
            }
			template {
				data = file("nomad/cert.tpl")
				destination = "local/cert.pem"
			}
			template {
				data = file("nomad/key.tpl")
				destination = "secrets/key.pem"
			}
            driver = "docker"
            config {
                image = var.image
                ports = ["app"]
                auth {
                    username = var.ci_user
                    password = var.ci_pass
                }
            }
        }
    }
}