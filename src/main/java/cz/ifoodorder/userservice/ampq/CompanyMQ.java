package cz.ifoodorder.userservice.ampq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

import org.springframework.amqp.core.Queue;

@Slf4j
public class CompanyMQ {
	private RabbitTemplate template;
	private Queue queue;

	public CompanyMQ(RabbitTemplate template, Queue queue) {
		this.template = template;
		this.queue = queue;
	}
	
	public void send(String companyId) {
		this.send(UUID.fromString(companyId));
	}
	
	public void send(UUID companyId) {
		template.convertAndSend(queue.getName(), companyId);
		log.info("Sent message to: {}", queue.getName());
	}
}
