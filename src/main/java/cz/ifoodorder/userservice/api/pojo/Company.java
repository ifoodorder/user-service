package cz.ifoodorder.userservice.api.pojo;

import lombok.Data;

@Data
public class Company {
	private String name;
}
