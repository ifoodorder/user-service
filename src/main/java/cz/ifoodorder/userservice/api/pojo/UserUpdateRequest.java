package cz.ifoodorder.userservice.api.pojo;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.Data;

@Data
public class UserUpdateRequest {
    private UUID id;
    private List<UserRole> roles;
    private String group;
    
    public List<String> getRawUserRoleNames() {
    	return roles.stream().map(UserRole::getName).collect(Collectors.toList());
    }
}
