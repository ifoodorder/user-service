package cz.ifoodorder.userservice.api.pojo;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserRole {
	public static final String USER = "user";
	public static final String RESTAURANT_ADMIN = "restaurant-admin";
	
	@JsonValue
	private String name;
}