package cz.ifoodorder.userservice.api;

import java.util.Collections;
import java.util.UUID;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ifoodorder.userservice.ampq.CompanyMQ;
import cz.ifoodorder.userservice.api.pojo.UserRole;
import cz.ifoodorder.userservice.keycloak.KeycloakApi;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UsersController {
	public static final ResponseEntity<Object> FORBIDDEN = ResponseEntity.status(403).build();
	private final KeycloakApi keycloakApi;
	private final CompanyMQ companyMq;
	
	@Autowired
	UsersController(KeycloakApi keycloakApi, CompanyMQ companyMq) {
		this.keycloakApi = keycloakApi;
		this.companyMq = companyMq;
	}
	
	@PutMapping("/{userId}/groups")
	public void createGroup(KeycloakAuthenticationToken auth, @PathVariable UUID userId) {
		log.debug("UserId: {}", userId);
		var isSameUser = UUID.fromString(auth.getName()).equals(userId);
		
		if(!isSameUser) {
			log.error("Cannot create group for different user: {}", auth.getName());
			throw new AccessDeniedException("Insufficient permissions!");
		}
		
		var groups = keycloakApi.getUsersGroups(userId);
		var hasGroups = !groups.isEmpty();
		var hasOneGroup = groups.size() == 1;
		
		if (hasOneGroup) {
			companyMq.send(groups.get(0));
		}
		
		if(hasGroups) {
			return;
		}
		
		var groupId = keycloakApi.createGroup();
		keycloakApi.addUserToGroup(userId, groupId);
		
		companyMq.send(groupId);
	}
	
	@PutMapping("/{userId}/roles/{roleName}")
	public void addRole(KeycloakAuthenticationToken auth, @PathVariable UUID userId, @PathVariable String roleName) {
		log.debug("UserID: {}, RoleName: {}", userId, roleName);
		boolean canAddRole = canAddRole(auth, userId);
		
		if (!canAddRole) {
			throw new AccessDeniedException("Insufficient permissions!");
		}
		assignRole(userId, roleName);
	}
	
	private boolean canAddRole(KeycloakAuthenticationToken auth, UUID targetUser) {
		UUID callerId = UUID.fromString(auth.getName());
		var realmAccess = getAccessToken(auth).getRealmAccess();
		if(realmAccess == null) {
			throw new IllegalStateException("Realm access shouldn't be null!");
		}
		
		boolean isAdmin = realmAccess.getRoles().contains(UserRole.RESTAURANT_ADMIN);
		var callerGroups = keycloakApi.getUsersGroups(callerId);
		var targetGroups = keycloakApi.getUsersGroups(targetUser);
		Collections.sort(callerGroups);
		Collections.sort(targetGroups);
		
		boolean hasSameGroups = callerGroups.equals(targetGroups);
		
		if(!hasSameGroups) {
			log.error("User {} is not in the same groups as user {}");
			return false;
		}
		
		if (!callerGroups.isEmpty() && !isAdmin) {
			log.error("Non admin user in a group cannot assign roles. Current roles: {}", realmAccess.getRoles());
			return false;
		}
		
		return true;
		
	}
	
	private AccessToken getAccessToken(KeycloakAuthenticationToken auth) {
		return ((RefreshableKeycloakSecurityContext)auth.getCredentials()).getToken();
	}
	
	private void assignRole(UUID userId, String roleName) {
		log.debug("UserId: {}", userId);
		log.debug("roleName: {}", roleName);
		var role = keycloakApi.getAvailableRoles(userId).stream()
				.filter(x -> x.getName().equals(roleName))
				.findFirst();
		role.ifPresent(x -> keycloakApi.addUserRole(userId, x));
	}
}
