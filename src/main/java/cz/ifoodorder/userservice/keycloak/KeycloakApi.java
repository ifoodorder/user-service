package cz.ifoodorder.userservice.keycloak;

import java.util.List;
import java.util.UUID;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

public interface KeycloakApi {
	List<UserRepresentation> getUsers();
	List<RoleRepresentation> getCurrentRoles(UUID user);
	List<RoleRepresentation> getAvailableRoles(UUID user);
	void addUserRoles(UUID user, List<RoleRepresentation> roles);
	void addUserRole(UUID user, RoleRepresentation role);
	UUID createGroup();
	List<String> getUsersGroups(UUID userId);
	void addUserToGroup(UUID userId, UUID groupId);
	boolean isUserInGroup(UUID userId);
}
	
