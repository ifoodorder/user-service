package cz.ifoodorder.userservice.keycloak;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.ToString;

@Data @ToString
@Configuration
@ConfigurationProperties(prefix = "ifoodorder.keycloak")
public class KeycloakApiProperties {
	private String realm;
	private String serviceName;
	private String serviceSecret;
	private String username;
	private String password;
}
