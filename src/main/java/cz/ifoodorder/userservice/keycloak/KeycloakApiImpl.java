package cz.ifoodorder.userservice.keycloak;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KeycloakApiImpl implements KeycloakApi {
	private final Keycloak keycloak;
	private final String realm;
	
	@Autowired
	public KeycloakApiImpl(KeycloakSpringBootProperties keycloakProperties,
			KeycloakApiProperties keycloakApiProperties,
			Keycloak keycloak) {
		log.debug("[KeycloakApiImpl]: KeycloakProperties: {}", keycloakProperties);
		this.realm = keycloakProperties.getRealm();
		this.keycloak = keycloak;
	}
	
	public List<UserRepresentation> getUsers() {
		return keycloak.realm(realm)
				.users()
				.list();
	}
	
	public List<RoleRepresentation> getCurrentRoles(UUID user) {
		log.debug("[getCurrentRoles]: UserId: {}", user);
		return keycloak.realm(realm)
				.users()
				.get(user.toString())
				.roles()
				.realmLevel()
				.listEffective();
	}
	public List<RoleRepresentation> getAvailableRoles(UUID user) {
		log.debug("[getAvailableRoles]: UserId: {}", user);
		return keycloak.realm(realm)
			.users()
			.get(user.toString())
			.roles()
			.realmLevel()
			.listAvailable();
	}
	
	public void addUserRoles(UUID user, List<RoleRepresentation> roles) {
		log.debug("[addUserRoles]: UserId: {}", user);
		log.debug("[addUserRoles]: RoleRepresentations: {}", roles);
		
		keycloak.realm(realm)
			.users()
			.get(user.toString())
			.roles()
			.realmLevel()
			.add(roles);
	}
	
	public void addUserRole(UUID user, RoleRepresentation role) {
		log.debug("[addUserRole]: UserId: {}", user);
		log.debug("[addUserRole]: Role: {}", role);
		addUserRoles(user, List.of(role));
	}
	
	private String getClientId(@NonNull String clientName) {
		log.debug("[getClientId]: ClientName: {}", clientName);
		log.debug("[getClientId]: Realm: {}", realm);
		
		var id =  keycloak.realm(realm)
				.clients()
				.findAll().stream()
				.peek(x -> log.debug("[getClientId]: clientName: {}", x.getName()))
				.filter(x -> x.getClientId().equals(clientName))
				.map(ClientRepresentation::getId)
				.findFirst()
				.orElseThrow();
		log.info("[getClientId]: Restaurant client id: {}", id);
		return id;
	}
	
	public UUID createGroup() {
		UUID initialName = UUID.randomUUID();
		GroupRepresentation group = new GroupRepresentation();
		group.setName(initialName.toString());
		
		keycloak.realm(realm)
			.groups()
			.add(group);
		
		var fetchedGroup = keycloak.realm(realm)
			.getGroupByPath("/"+ initialName.toString());
		
		var keycloakId = fetchedGroup.getId();
		
		fetchedGroup.setName(keycloakId);
		
		keycloak.realm(realm)
			.groups()
			.group(keycloakId)
			.update(fetchedGroup);
		
		return UUID.fromString(keycloakId);
	}
	
	public List<String> getUsersGroups(UUID userId) {
		log.debug("[getUsersGroups]: UserId: {}", userId);
		return keycloak.realm(realm)
			.users()
			.get(userId.toString())
			.groups()
			.stream()
			.map(GroupRepresentation::getId)
			.collect(Collectors.toList());
	}

	public void addGroup(UUID groupId) {
		log.debug("[addGroup]: GroupId: {}", groupId);
		keycloak.realm(realm)
			.addDefaultGroup(groupId.toString());
	}
	
	public void addGroup(GroupRepresentation groupRepresentation) {
		log.debug("[addGroup]: GroupRepresentation: {}", groupRepresentation);
		keycloak.realm(realm)
			.groups()
			.add(groupRepresentation);
	}
	
	public void addUserToGroup(UUID userId, UUID groupId) {
		log.debug("[addUserToGroup]: UserId: {}", userId);
		log.debug("[addUserToGroup]: GroupId: {}", groupId);
		keycloak.realm(realm)
			.users()
			.get(userId.toString())
			.joinGroup(groupId.toString());
	}

	public boolean isUserInGroup(UUID userId) {
		log.debug("[isUserInGroup]: UserId: {}", userId);
		return !keycloak.realm(realm)
				.users()
				.get(userId.toString())
				.groups()
				.isEmpty();
	}
}
