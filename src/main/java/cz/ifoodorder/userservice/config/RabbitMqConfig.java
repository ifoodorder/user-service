package cz.ifoodorder.userservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cz.ifoodorder.userservice.ampq.CompanyMQ;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@Configuration
public class RabbitMqConfig {
	private RabbitTemplate template;
	
	public RabbitMqConfig(RabbitTemplate template) {
		this.template = template;
	}
	
	@Bean
	public Queue company() {
		return new Queue("company", true);
	}
	
	@Bean
	public CompanyMQ sender() {
		return new CompanyMQ(template, company());
	}
}
