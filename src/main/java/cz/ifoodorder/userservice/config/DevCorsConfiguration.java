package cz.ifoodorder.userservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Profile({"dev", "docker"})
public class DevCorsConfiguration implements WebMvcConfigurer {
	@Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
        	.addMapping("/**")
        	.allowedOriginPatterns("http://localhost:*", "https://localhost:*", "http://192.168.*.*:*")
        	.allowedHeaders("*")
        	.allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS")
        	.allowCredentials(true);
    }
}
