package cz.ifoodorder.userservice.config;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.adapters.config.BaseRealmConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.keycloak.OAuth2Constants;

import cz.ifoodorder.userservice.keycloak.KeycloakApiProperties;

@Configuration
public class KeycloakAdminApiConfig {
	private KeycloakApiProperties adminConfig;
	private BaseRealmConfig keycloakConfig;
	
	public KeycloakAdminApiConfig(KeycloakApiProperties adminConfig, BaseRealmConfig keycloakConfig) {
		this.adminConfig = adminConfig;
		this.keycloakConfig = keycloakConfig;
	}
	
	@Bean
	Keycloak keycloakAdminApi() {
		return KeycloakBuilder.builder()
				.grantType(OAuth2Constants.PASSWORD)
				.serverUrl(keycloakConfig.getAuthServerUrl())
				.realm(keycloakConfig.getRealm())
				.clientId(adminConfig.getServiceName())
				.clientSecret(adminConfig.getServiceSecret())
				.username(adminConfig.getUsername())
				.password(adminConfig.getPassword())
				.build();
	}
}
