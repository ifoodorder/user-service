server {
	server_name _;
	listen ${HTTP_PORT:-80};
	listen [::]:${HTTP_PORT:-80};

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	location / {
		proxy_pass	http://127.0.0.1:${JAVA_PORT:-5051};
		proxy_set_header    Host               \$host;
		proxy_set_header    X-Real-IP          \$remote_addr;
		proxy_set_header    X-Forwarded-For    \$proxy_add_x_forwarded_for;
		proxy_set_header    X-Forwarded-Host   \$host;
		proxy_set_header    X-Forwarded-Server \$host;
		proxy_set_header    X-Forwarded-Port   \$server_port;
		proxy_set_header    X-Forwarded-Proto  \$scheme;
	}
}
server {
	server_name _;
	listen  ${HTTPS_PORT:-443} ssl http2 _;
	listen  [::]:${HTTPS_PORT:-443} ssl http2 _;

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	ssl_certificate ${CERT_PATH:-/tmp/snakeoil-cert.pem};
	ssl_certificate_key ${CERT_KEY_PATH:-/tmp/snakeoil-key.pem};

	# Copied from let's encrypt config:
	ssl_session_cache   shared:le_nginx_SSL:10m;
	ssl_session_timeout 1440m;
	ssl_session_tickets off;
	ssl_protocols   TLSv1.2 TLSv1.3;
	ssl_prefer_server_ciphers   off;
	ssl_ciphers "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";

	# ssl_dhparam /tmp/dhparams.pem;

	location / {
		proxy_pass	http://127.0.0.1:${JAVA_PORT:-5051};
		proxy_set_header    Host               \$host;
		proxy_set_header    X-Real-IP          \$remote_addr;
		proxy_set_header    X-Forwarded-For    \$proxy_add_x_forwarded_for;
		proxy_set_header    X-Forwarded-Host   \$host;
		proxy_set_header    X-Forwarded-Server \$host;
		proxy_set_header    X-Forwarded-Port   \$server_port;
		proxy_set_header    X-Forwarded-Proto  \$scheme;
	}
}
