#!/bin/bash

echo "Generating snakeoil certificate"
openssl req -x509 -newkey rsa:4096 -keyout /tmp/snakeoil-key.pem -out /tmp/snakeoil-cert.pem -days 365 -nodes -subj '/CN=localhost'

echo "Evaluating nginx template"
NGINX_CONF="/etc/nginx/http.d/service.conf"
rm -f /etc/nginx/http.d/default.conf
eval "cat <<EOF
$(</etc/nginx/templates/service.conf.tpl)
EOF
" 2> /dev/null > ${NGINX_CONF}


echo "Testing nginx config"
nginx -t

echo "Starting nginx"
nginx -g "daemon on;"

java -jar /app.jar