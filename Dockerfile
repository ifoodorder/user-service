FROM adoptopenjdk/openjdk11:alpine-jre
RUN addgroup -S ifoodorder \
    && adduser -S ifoodorder -G ifoodorder \
    && apk update \
    && apk add nginx openssl bash \
    && mkdir /etc/nginx/templates/ \
    && mkdir /run/nginx

COPY docker/service.conf.tpl /etc/nginx/templates/service.conf.tpl
COPY docker/docker-entrypoint.sh /docker-entrypoint.sh
COPY target/*.jar app.jar

RUN chmod +x /docker-entrypoint.sh \
    && chown ifoodorder:ifoodorder /docker-entrypoint.sh \
    && chown -R ifoodorder:ifoodorder /etc/nginx \
    && chown -R ifoodorder:ifoodorder /var/lib/nginx \
    && chown -R ifoodorder:ifoodorder /var/log/nginx \
    && chown -R ifoodorder:ifoodorder /run/nginx


USER ifoodorder:ifoodorder
ENV HTTP_PORT 80
ENV HTTPS_PORT 443

ENV RESTAURANT_HOST http://restaurant-service:8050
ENV MENU_HOST http://menu-service:8053
ENV KEYCLOAK_SECRET 98585d22-4e4f-4e1c-9122-9890be29bbde
ENV KEYCLOAK_HOST http://keycloak:8080
ENV POSTGRESQL_HOST jdbc:postgresql://postgresql:5432/user-service

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/docker-entrypoint.sh"]