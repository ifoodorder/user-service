# nginx config
HTTPS_PORT = ${NOMAD_PORT_app}
CERT_PATH = /local/cert.pem
CERT_KEY_PATH = /secrets/key.pem

# java config
JAVA_PORT = 5051
{{- with $instances := service "keycloak" }}
    {{- with $keycloak := index $instances 0 }}
KEYCLOAK_URL = "https://keycloak.service.consul:{{ $keycloak.Port }}/auth"
    {{- end }}
{{- end }}
KEYCLOAK_RESOURCE = Restaurant
KEYCLOAK_SECRET = {{ with secret "secret/user/prod" }}{{ .Data.USER_KEYCLOAK_SECRET }}{{ end }}
JDBC_URL = "jdbc:postgresql://postgresql.service.consul:5432/user"
JDBC_USER = {{ with secret "/database/creds/user" }}{{ .Data.username }}
JDBC_PWD = {{ .Data.password }}{{ end }}
H2_CONSOLE = false
RABBITMQ_HOST = "rabbitmq.service.consul"
RABBITMQ_PORT = "5671"
RABBITMQ_VIRTUALHOST = ifoodorder
SQL_INIT = never
LOG_FILE = logs/user-service.log
LOG_LEVEL = INFO

{{- with $instances := service "restaurant-service" }}
    {{- with $restaurant := index $instances 0 }}
RESTAURANT_HOST = "https://restaurant-service.service.consul:{{ $restaurant.Port }}"
    {{- end }}
{{- end }}

{{- with $instances := service "menu-service" }}
    {{- with $menu := index $instances 0 }}
MENU_HOST = "https://restaurant-service.service.consul:{{ $menu.Port }}"
    {{- end }}
{{- end }}


KEYCLOAK_MANAGEMENT_USER = {{ with secret "secret/user/prod" }}{{ .Data.USER_MANAGEMENT_KEYCLOAK_USER }}
KEYCLOAK_MANAGEMENT_PASSWORD = {{ .Data.USER_MANAGEMENT_KEYCLOAK_PWD }}
KEYCLOAK_MANAGEMENT_SECRET = {{ .Data.USER_MANAGEMENT_KEYCLOAK_SECRET }}{{ end }}